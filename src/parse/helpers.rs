pub fn bin_to_u32(binary: &[u8]) -> usize {
    binary.iter().take(4).fold(0us, |acc, x| acc * 256 + (*x as usize))
}

#[test]
fn bin_to_u32_test() {
    assert_eq!(0,   bin_to_u32(&[0u8, 0, 0, 0]));
    assert_eq!(2,   bin_to_u32(&[0u8, 0, 0, 2]));
    assert_eq!(256, bin_to_u32(&[0u8, 0, 1, 0]));
    assert_eq!(258, bin_to_u32(&[0u8, 0, 1, 2]));
    // skip additional bytes
    assert_eq!(258, bin_to_u32(&[0u8, 0, 1, 2, 1, 3, 5, 1]));
}

pub fn align(n: usize) -> usize {
    if n % 4 == 0 { n } else { n / 4 * 4 + 4 }
}

#[test]
fn align_test() {
    assert_eq!(0,  align(0));
    assert_eq!(4,  align(1));
    assert_eq!(4,  align(4));
    assert_eq!(48, align(47));
}
