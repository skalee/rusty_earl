use parse::helpers;

/// Iterates table. 4 first bytes is the number of items
pub struct TableIterator<'bin> {
    mode: Mode,
    binary: &'bin [u8],
    binary_idx: usize,
    remaining: usize
}

pub enum Mode {
    String,
    Function,
}

impl<'bin> Iterator for TableIterator<'bin> {
    type Item = &'bin [u8];

    #[inline(always)]
    fn next(&mut self) -> Option<&'bin [u8]> {
        if self.remaining > 0 {
            let(slice, advance) = self.get_next_item();
            self.remaining -= 1;
            self.binary_idx += advance;
            Some(slice)
        } else {
            None
        }
    }
}

impl<'bin> TableIterator<'bin> {
    fn new(binary: & [u8], mode: Mode) -> TableIterator {
        let count = helpers::bin_to_u32(binary);
        TableIterator{ binary: binary, binary_idx: 4, remaining: count, mode: mode }
    }

    fn get_next_item(&self) -> (&'bin [u8], usize) {
        match self.mode {
            Mode::String => {
                let item_size = self.binary[self.binary_idx] as usize;
                let from = self.binary_idx + 1;
                let to   = self.binary_idx + 1 + item_size;
                let slice = &self.binary[from .. to];
                let advance = item_size + 1;
                (slice, advance)
            }
            Mode::Function => {
                let item_size = 12;
                let slice = &self.binary[self.binary_idx .. self.binary_idx + item_size];
                let advance = item_size;
                (slice, advance)
            }
        }
    }
}

pub fn iterate_table(binary: &[u8], mode: Mode) -> TableIterator {
    TableIterator::new(binary, mode)
}

#[test]
fn iterate_string_table_test() {
    let binary: &[u8] = &[
        0u8, 0, 0, 2,
        2, b'S', b'T',
        3, b'R', b'I', b'N',
        b'G' // junk
    ];
    let mut iter = iterate_table(binary, Mode::String);
    assert_eq!("ST".as_bytes(),  iter.next().unwrap());
    assert_eq!("RIN".as_bytes(), iter.next().unwrap());
}

#[test]
fn iterate_function_table_test() {
    let binary: &[u8] = &[
        0u8, 0, 0, 2,
        0u8, 1, 2, 3,  2, 3, 4, 5,  0, 0, 0, 1,
        4u8, 1, 2, 3,  2, 3, 4, 5,  0, 0, 0, 2,
        0u8, 1, 2, 3 // junk
    ];
    let mut iter = iterate_table(binary, Mode::Function);
    assert_eq!([0u8, 1, 2, 3,  2, 3, 4, 5,  0, 0, 0, 1], iter.next().unwrap());
    assert_eq!([4u8, 1, 2, 3,  2, 3, 4, 5,  0, 0, 0, 2], iter.next().unwrap());
}
