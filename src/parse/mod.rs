use lang::Atom;
use module::Module;
use node::LoadResult;
use node::Node;
use std::str;

struct ParserState<'a> {
    node: &'a mut Node,
    atoms: Vec<Atom>,
    module: Box<Module>,
}

pub fn recognize(node: &mut Node, binary: &[u8]) -> LoadResult {
    if  ! ("FOR1".as_bytes() == &binary[0 .. 4]) ||
        ! ("BEAM".as_bytes() == &binary[8 .. 12]) {
            return Err("Not a BEAM file")
    }

    debug!("Recognized BEAM form");
    let mut parser = ParserState::new(node);
    parser.recognize_chunks(&binary[12 ..]);
    debug!("{}", parser.node.stats());
    Ok(parser.module)
}

impl<'a> ParserState<'a> {

    fn new(node: &'a mut Node) -> ParserState {
        let module = box Module::new(node);
        ParserState{ node: node, atoms: Vec::new(), module: module }
    }

    fn recognize_chunks(&mut self, binary: &[u8]) {
        if binary.len() == 0 { return }

        let id  = str::from_utf8(&binary[0 .. 4]);
        let len = helpers::bin_to_u32(&binary[4 .. 8]);
        let chunk_body = &binary[8 .. 8 + len];

        match id {
            Ok("Atom") => self.process_atom_table(chunk_body),
            Ok("ExpT") => self.process_export_table(chunk_body),
            Ok("ImpT") => self.process_import_table(chunk_body),
            _ => (), // skip
        }

        self.recognize_chunks(&binary[8 + helpers::align(len) ..])
    }

    fn process_atom_table(&mut self, binary: &[u8]) {
        debug!("BEAM chunk found: {}, size: {}", "atom table", binary.len());

        // atoms in BEAMs are indexed from 1, push any dummy item then
        self.atoms.push(self.node.atoms.get("erlang"));

        let iterator = table_iterator::iterate_table(binary, table_iterator::Mode::String);
        for atom_name_slice in iterator {
            let atom_name = str::from_utf8(atom_name_slice).unwrap();
            let atom = self.node.atoms.get(atom_name);
            self.atoms.push(atom);
        }
    }

    #[allow(unused_variables)]
    fn process_export_table(&mut self, binary: &[u8]) {
        debug!("BEAM chunk found: {}, size: {}", "export table", binary.len());

        let iterator = table_iterator::iterate_table(binary, table_iterator::Mode::Function);
        for function_slice in iterator {
            let function = self.atoms[helpers::bin_to_u32(&function_slice[0 .. 4])];
            let arity = helpers::bin_to_u32(&function_slice[4 .. 8]);
            let code = helpers::bin_to_u32(&function_slice[8 .. 12]);

            let ref mut export = self.module.export;
            let function_name = (function, arity);
            export.insert(function_name);
        }
    }

    #[allow(unused_variables)]
    fn process_import_table(&mut self, binary: &[u8]) {
        debug!("BEAM chunk found: {}, size: {}", "import table", binary.len());

        let iterator = table_iterator::iterate_table(binary, table_iterator::Mode::Function);
        for function_slice in iterator {
            let module = self.atoms[helpers::bin_to_u32(&function_slice[0 .. 4])];
            let function = self.atoms[helpers::bin_to_u32(&function_slice[4 .. 8])];
            let arity = helpers::bin_to_u32(&function_slice[8 .. 12]);

            let ref mut import = self.module.import;
            let fully_qualified_name = (module, (function, arity));
            import.insert(fully_qualified_name);
        }
    }

}

mod helpers;
mod table_iterator;

#[test]
fn process_atom_table_test() {
    // 2 atoms: ST and RIN
    let binary: &[u8] = &[
        0u8, 0, 0, 2,
        2u8, b'S', b'T',
        3u8, b'R', b'I', b'N',
        b'G', // junk
    ];
    let mut node = Node::new();
    let mut parser = ParserState::new(&mut node);
    parser.process_atom_table(binary);
    assert_eq!(3, parser.atoms.len());
    assert_eq!(parser.node.atoms.get("ST"),  parser.atoms[1]);
    assert_eq!(parser.node.atoms.get("RIN"), parser.atoms[2]);
}

#[test]
fn process_export_table_test() {
    let binary: &[u8] = &[
        0u8, 0, 0, 2,
        0u8, 0, 0, 1,   0, 0, 0, 1,   2, 3, 4, 5,
        0u8, 0, 0, 3,   0, 0, 0, 2,   2, 3, 4, 5,
        0u8, 1, 2, 3 // junk
    ];
    let mut node = Node::new();
    let mut parser = ParserState::new(&mut node);
    parser.atoms.push_all(&[0, 1, 2, 3]);
    parser.process_export_table(binary);

    let exports = parser.module.export;
    assert_eq!(exports.len(), 2);
    assert!(exports.contains(&(1, 1)));
    assert!(exports.contains(&(3, 2)));
}

#[test]
fn process_import_table_test() {
    let binary: &[u8] = &[
        0u8, 0, 0, 2,
        0u8, 0, 0, 1,   0, 0, 0, 1,   0, 0, 0, 5,
        0u8, 0, 0, 3,   0, 0, 0, 2,   0, 0, 0, 5,
        0u8, 1, 2, 3 // junk
    ];
    let mut node = Node::new();
    let mut parser = ParserState::new(&mut node);
    parser.atoms.push_all(&[0, 1, 2, 3]);
    parser.process_import_table(binary);

    let imports = parser.module.import;
    assert_eq!(imports.len(), 2);
    assert!(imports.contains(&(1, (1, 5))));
    assert!(imports.contains(&(3, (2, 5))));
}
