#![feature(core)]
#![feature(env)]
#![feature(os)]
#![feature(path)]

extern crate rusty_earl;

use std::env;

#[cfg(not(test))]
fn main() {
    let mut node = rusty_earl::node::Node::new();

    for filename in env::args().skip(1) {
        let path = Path::new(filename.into_string().unwrap());
        match node.load_module_from_path(path) {
            Err(err) => println!("Could not load: {}", err),
            Ok(m_ref) => (*m_ref).inspect(),
        }
    }
}
