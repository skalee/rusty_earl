pub type Atom = usize;

/// Function signature: (name, arity)
pub type ShortFn = (Atom, usize);

/// Fully qualified function signature: (module, (name, arity))
pub type LongFn = (Atom, ShortFn);

pub type OpCode = u8;
pub type Code = Vec<Operation>;

pub struct Operation {
    pub opcode: OpCode,
    pub arguments: (u8, u8, u8),
}
