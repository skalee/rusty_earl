use atoms::AtomTable;
use module::Module;
use parse;

use std::old_io::File;
use std::rc::Rc;

pub type LoadResult = Result<Box<Module>, LoadError>;
pub type LoadError = &'static str;

/// Node instance capable of running Erlang programs.
pub struct Node {
    pub atoms: Rc<AtomTable>,
}

impl Node {

    /// Instantiates new Node
    pub fn new() -> Node {
        let instance = Node{
            atoms: Rc::new(AtomTable::new()),
        };
        info!("New node booted");
        instance
    }

    pub fn stats(self: &Node) -> String {
        format!("Atoms: {}", self.atoms.len())
    }

    #[cfg(not(test))]
    pub fn load_module_from_path(&mut self, path: Path) -> LoadResult {
        match File::open(&path).read_to_end() {
            Ok(binary) => self.load_module_from_binary(binary),
            Err(e) => Err(e.desc)
        }
    }

    #[cfg(not(test))]
    pub fn load_module_from_binary(&mut self, binary: Vec<u8>) -> LoadResult {
        debug!("Attempting to load BEAM");
        let result = parse::recognize(self, binary.as_slice());
        if result.is_ok() { info!("New module loaded") };
        result
    }

}

#[test]
fn new_test() {
    let node = Node::new();
    println!("Allocated: {:p}", &node);
    // should not fail
}

#[test]
fn stats_test() {
    let mut node = Node::new();
    node.atoms.get("atom");
    assert_eq!("Atoms: 1", node.stats());
}
