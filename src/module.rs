use atoms::AtomTable;
use lang::LongFn;
use lang::Operation;
use lang::ShortFn;
use node::Node;
use std::collections::HashSet;
use std::rc::Rc;

pub struct Module {
    atoms: Rc<AtomTable>,
    pub code: Vec<Operation>,
    pub export: HashSet<ShortFn>,
    pub import: HashSet<LongFn>,
}

impl Module {

    pub fn new(node: &Node) -> Module {
        Module {
            atoms: node.atoms.clone(),
            code: Vec::new(),
            export: HashSet::new(),
            import: HashSet::new(),
        }
    }

    pub fn inspect(&self) {
        println!("Imports:");
        for &(module, (name, arity)) in self.import.iter() {
            let name_str = self.atoms.name_of(name);
            let module_str = self.atoms.name_of(module);
            println!("  {}:{}/{}", module_str, name_str, arity);
        }
        println!("Exports:");
        for &(name, arity) in self.export.iter() {
            let name_str = self.atoms.name_of(name);
            println!("  {}/{}", name_str, arity);
        }
    }

}
