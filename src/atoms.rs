use std::cell::RefCell;
use std::collections::HashMap;
use std::mem::copy_lifetime;
use lang::Atom;

pub struct AtomTable {
    name_to_atom: RefCell<HashMap<String, Atom>>,
    atom_to_name: RefCell<HashMap<Atom, String>>,
}

impl AtomTable {

    /// Adds atom name to table if missing.  Returns associated int.
    pub fn get(self: &AtomTable, atom_name: &str) -> Atom {
        let maybe_atom = self.get_stored_by_name(atom_name);
        maybe_atom.unwrap_or_else(|| self.add(atom_name))
    }

    /// Gets name: &str associated with given Atom
    pub fn name_of<'a>(self: &'a AtomTable, atom: Atom) -> &'a str {
        let b = self.atom_to_name.borrow();
        let s = b.get(&atom).unwrap().as_slice();
        unsafe { copy_lifetime(self, s) }
    }

    pub fn len(&self) -> usize {
        self.name_to_atom.borrow().len()
    }

    // TODO: Optimize
    fn add(self: &AtomTable, atom_name: &str) -> Atom {
        let mut atom_to_name = self.atom_to_name.borrow_mut();
        let mut name_to_atom = self.name_to_atom.borrow_mut();
        let int_val = name_to_atom.len();
        let allocated_1 = String::from_str(atom_name);
        let allocated_2 = String::from_str(atom_name);
        atom_to_name.insert(int_val, allocated_1);
        name_to_atom.insert(allocated_2, int_val);
        int_val
    }

    fn get_stored_by_name(self: &AtomTable, atom_name: &str) -> Option<Atom> {
        let b = self.name_to_atom.borrow();
        b.get(atom_name).and_then(|v| Some(*v))
    }

    pub fn new() -> AtomTable {
        AtomTable {
            name_to_atom: RefCell::new(HashMap::new()),
            atom_to_name: RefCell::new(HashMap::new()),
        }
    }

}

#[test]
fn translates_names_to_identities() {
    let mut table = AtomTable::new();
    assert!(table.get("atom") == table.get("atom"));
    assert!(table.get("atom") != table.get("another"));
}
