Example implementation of Erlang VM in Rust.  It's an excercise,
not suitable for production use.  Currently able to parse BEAM files but
not yet to run code.

### Usage

1.  Visit http://www.rust-lang.org/install.html
2.  Grab Rust, make sure Cargo is available with `which cargo`
3.  Build with `cargo build`
4.  Run tests with `cargo test` (lots of dead code warnings, some portion of
    code is hard to test and unused in this mode, ignore them)
5.  Parse example BEAM file:
    `env RUST_LOG=debug RUST_BACKTRACE=1 cargo run tests/fixtures/fib.beam`

### License

Open source (ISC).
