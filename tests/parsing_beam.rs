#![feature(path)]

extern crate rusty_earl;

use rusty_earl::module::Module;
use rusty_earl::node::Node;

#[test]
fn fails_on_misspelled_path() {
    let mut node = Node::new();
    let beam_path = Path::new("./tests/fixtures/fib_bad.beam");
    let md_result = node.load_module_from_path(beam_path);
    assert!(md_result.is_err());
}

#[test]
fn loads_correct_beam() {
    let mut node = Node::new();
    let beam_path = Path::new("./tests/fixtures/fib.beam");
    let md_result = node.load_module_from_path(beam_path);
    assert!(md_result.is_ok());
}
